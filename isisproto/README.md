# Apache Isis Prototype App

This chart allows an arbitrary Apache Isis application to be deployed as a prototype (using an in-memory database), to the `incode.cloud` k8s cluster.

The chart is intended to pull from a private Docker registry, and requires a corresponding Docker secret to be created into the cluster to allow the images to be pulled.

## tl;dr

    helm upgrade --install isis-demoapp-simpleapp         \
        incodehq-public/isisproto --version 0.0.3         \
        --set appname="isis-demoapp-simpleapp"            \
        --set dnsname="simpleapp.incode.cloud"           \
        --set image.name="simpleapp"                      \
        --set image.tag="2.0.0-M1.20180430-2102-fb88e0b3"

This will deploy the specified image using the release name of "isis-demoapp-simpleapp".
If there is a release already then it will be updated.
 
A CNAME entry must be defined for the `dnsname` variable.
(On AKS, this is something like `incode.cloud`, corresponding to an nginx server which can be installed using `helm install stable/nginx-ingress`).


The Docker image is pulled from a Docker registry defaulted from `values.yaml`.
This can be overridden using additional `--set` properties.


## Prerequisites/assumptions

This chart requires:

* that an (Nginx) Ingress Controller has previously been deployed, with SSL enabled via Kube-Lego
* that a Docker secret has been created in the cluster to allow the images to be pulled from the specified Docker registry.

The `values.yml` defaults are:

* `image.registry`: "docker-dev.incode.cloud"
* `image.owner`: "incodehq-public"
* `image.pull.secret.name`: "docker-dev-incode-cloud-incodehq-public"

The secret can be installed using something like:

    kubectl create secret docker-registry       \
        docker-dev-incode-cloud-incodehq-public \
        --docker-server=docker-dev.incode.cloud \
        --docker-username=incodehq-public       \
        --docker-password="CHANGEME"            \
        --docker-email="changeme@changeme.com"


## Usage
    
The Helm chart defines three mandatory properties that MUST be supplied:

* `appname`
* `dnsname`
* `image.name`
* `image.tag`

To deploy (with all other configurable values defaults using the chart's own `values.yaml` file):

    helm upgrade --install isis-demoapp-simpleapp         \
        incodehq-public/isisproto                         \
        --version 0.0.3                                   \
        --set appname="isis-demoapp-simpleapp"            \
        --set dnsname="simpleapp.incode.cloud"           \
        --set image.name="simpleapp"                      \
        --set image.tag="2.0.0-M1.20180518-1121-69b64ab4"


This will pull:

* from the `docker-dev.incode.cloud` registry
* the `incodehq-public/${image.name}/${image.tag}` image
* using the credentials defined by the `docker-dev-incode-cloud-incodehq-public` k8s secret.

and should result in something like:

                        `
    NAME:   isis-demoapp-simpleapp
    LAST DEPLOYED: Fri May 18 14:30:59 2018
    NAMESPACE: default
    STATUS: DEPLOYED
    
    RESOURCES:
    ==> v1/Service
    NAME                    TYPE       CLUSTER-IP  EXTERNAL-IP  PORT(S)  AGE
    isis-demoapp-simpleapp  ClusterIP  10.0.54.50  <none>       80/TCP   0s
    
    ==> v1beta1/Deployment
    NAME                    DESIRED  CURRENT  UP-TO-DATE  AVAILABLE  AGE
    isis-demoapp-simpleapp  1        1        1           0          0s
    
    ==> v1beta1/Ingress
    NAME                    HOSTS                                ADDRESS  PORTS  AGE
    isis-demoapp-simpleapp  isis-demoapp-simpleapp.incode.cloud  80, 443  0s
    
    ==> v1/Pod(related)
    NAME                        READY  STATUS             RESTARTS  AGE
    simpleapp-5498d6d58c-jw55t  0/1    ContainerCreating  0         0s
    
    
    NOTES:
    Your release is named isis-demoapp-simpleapp.
    
    To learn more about the release, try:
    
      $ helm status isis-demoapp-simpleapp
      $ helm get isis-demoapp-simpleapp
    

To deploy from a different registry, and a different image owner, with a different secret:

    helm upgrade --install isis-demoapp-simpleapp         \
        incodehq-public/isisproto                         \
        --version 0.0.2                                   \
        --set appname="isis-demoapp-simpleapp"            \
        --set dnsname="simpleapp.incode.cloud"           \
        --set image.name="simpleapp"                      \
        --set image.tag="2.0.0-M1.20180518-1121-69b64ab4" \
        --set image.registry="docker-test.incode.cloud"   \
        --set image.owner="incodehq-private"              \
        --set image.pull.secret.name="docker-test-incode-cloud-incodehq-private"

This will pull:

* from the `docker-test.incode.cloud` registry
* the `incodehq-private/${image.name}/${image.tag}` image
* using the credentials defined by the `docker-test-incode-cloud-incodehq-private` k8s secret.
